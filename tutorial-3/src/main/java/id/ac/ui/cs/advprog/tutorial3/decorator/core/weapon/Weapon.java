package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.springframework.stereotype.Service;

@Service
public abstract class Weapon {

    protected String weaponName;
    protected String weaponDescription;
    protected int weaponValue;

    public String getName(){
        return weaponName;
    }

    public int getWeaponValue(){
        return weaponValue;
    }

    public String getDescription(){
        return weaponDescription;
    }

}
