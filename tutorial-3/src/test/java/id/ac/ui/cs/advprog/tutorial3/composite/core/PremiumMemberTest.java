package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member anak = new PremiumMember("Ariell", "Premium");
        member.addChildMember(anak);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member anak = new PremiumMember("Ariell", "Premium");
        member.addChildMember(anak);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(anak);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new PremiumMember("Ariell", "Premium"));
        member.addChildMember(new PremiumMember("Ariell", "Premium"));
        member.addChildMember(new PremiumMember("Ariell", "Premium"));
        member.addChildMember(new PremiumMember("Ariell", "Premium"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member gm = new PremiumMember("Ariell", "Master");
        gm.addChildMember(new PremiumMember("Riell", "Premium"));
        gm.addChildMember(new PremiumMember("Riell", "Premium"));
        gm.addChildMember(new PremiumMember("Riell", "Premium"));
        gm.addChildMember(new PremiumMember("Riell", "Premium"));
        assertEquals(4, gm.getChildMembers().size());
    }
}
