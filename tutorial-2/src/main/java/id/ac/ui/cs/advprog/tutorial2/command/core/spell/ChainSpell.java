package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    ArrayList<Spell> arr;

    public ChainSpell(ArrayList<Spell> arr) {
        this.arr = arr;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (int k = 0 ; k < arr.size() ; k++){
            arr.get(k).cast();
        }
    }

    @Override
    public void undo() {
        for (int k = arr.size()-1 ; k > -1 ; k--){
            arr.get(k).cast();
        }
    }
}
