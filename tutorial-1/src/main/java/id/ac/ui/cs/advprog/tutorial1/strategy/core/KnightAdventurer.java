package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me

    private String alias;

    public KnightAdventurer(){
        this.alias = "KNIGHT";
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    public String getAlias(){
        return this.alias;
    }
}
