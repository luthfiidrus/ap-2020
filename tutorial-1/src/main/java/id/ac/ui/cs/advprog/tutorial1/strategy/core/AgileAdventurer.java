package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete me
    private String alias;

    public AgileAdventurer(){
        this.alias = "AGILE";
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    public String getAlias(){
        return this.alias;
    }
}
