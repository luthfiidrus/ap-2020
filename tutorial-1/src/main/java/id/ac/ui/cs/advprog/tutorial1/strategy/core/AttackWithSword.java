package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me

    public String attack(){
        return "Attacking with sword!";
    }

    public String getType(){
        return "SWORD";
    }
}
